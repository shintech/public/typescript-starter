import nock from 'nock';
import request, { HttpResponse } from 'requestr';

const PKG_NAME = process.env['npm_package_name'];
const PKG_VERSION = process.env['npm_package_version'];

interface Payload {
  id: number;
  message?: string;
}

beforeEach(() => {
  jest.resetModules();
  nock.cleanAll.bind(nock);

  nock('https://localhost:8080')
    .get('/path')
    .reply(200, {
      message: {
        name: PKG_NAME,
        version: PKG_VERSION
      }
    });

  nock('http://localhost:8080')
    .get('/path')
    .query({
      id: 'test'
    })
    .reply(200, {
      id: 'test'
    })

    .get('/path')
    .reply(200, {
      message: {
        name: PKG_NAME,
        version: PKG_VERSION
      }
    })

    .post('/path')
    .reply(200, (_uri, body: Payload | string) => {
      if (typeof body === 'string') {
        return {
          message: body
        };
      } else {
        return body;
      }
    })

    .get('/path/redirect')
    .reply(302, undefined, {
      Location: 'http://localhost:8080/path'
    })

    .put('/path/1')
    .reply(200, (_uri, body: Payload) => ({
      id: body.id,
      message: body.message
    }))

    .delete('/path/1')
    .reply(200, (_uri, body: Payload) => ({
      id: body.id
    }));
});

afterAll(() => {
  nock.restore();
});

describe('Request Module - GET - index -> /path', () => {
  let res: HttpResponse;

  beforeEach(async () => {
    res = await request('GET', 'http://localhost:8080/path');
  });

  test('expect response to have property "statusCode"', () => {
    expect(res).toHaveProperty('statusCode');
  });

  test('expect response to have property "body"', () => {
    expect(res).toHaveProperty('body');
  });

  test('expect response to have property "method"', () => {
    expect(res).toHaveProperty('method');
  });

  test('expect response to have property "url"', () => {
    expect(res).toHaveProperty('url');
  });

  test('expect response to have property "statusMessage"', () => {
    expect(res).toHaveProperty('statusMessage');
  });

  test('expect response to have property "headers"', () => {
    expect(res).toHaveProperty('headers');
  });

  test('expect response to have property "rawHeaders"', () => {
    expect(res).toHaveProperty('rawHeaders');
  });

  test('expect response to have property "socket"', () => {
    expect(res).toHaveProperty('socket');
  });

  test('expect response.body to have property "message"', () => {
    expect(res.body).toHaveProperty('message');
  });

  test('expect response.body.message to have property "name"', () => {
    expect(res.body?.message).toHaveProperty('name');
  });

  test('expect response.body.message to have property "version"', () => {
    expect(res.body?.message).toHaveProperty('version');
  });

  test('expect response.statusCode to be "200"', () => {
    expect(res.statusCode).toBe(200);
  });

  test(`expect response.body.message.name to be "${PKG_NAME}"`, () => {
    expect(res.body?.message.name).toBe(PKG_NAME);
  });

  test(`expect response.body.message.version to be "${PKG_VERSION}"`, () => {
    expect(res.body?.message.version).toBe(PKG_VERSION);
  });
});

describe('Request Module - GET - index -> /path - secure', () => {
  let res: HttpResponse;

  beforeEach(async () => {
    res = await request('GET', 'https://localhost:8080/path', { secure: true });
  });

  test('expect response to have property "statusCode"', () => {
    expect(res).toHaveProperty('statusCode');
  });

  test('expect response to have property "body"', () => {
    expect(res).toHaveProperty('body');
  });

  test('expect response to have property "method"', () => {
    expect(res).toHaveProperty('method');
  });

  test('expect response to have property "url"', () => {
    expect(res).toHaveProperty('url');
  });

  test('expect response to have property "statusMessage"', () => {
    expect(res).toHaveProperty('statusMessage');
  });

  test('expect response to have property "headers"', () => {
    expect(res).toHaveProperty('headers');
  });

  test('expect response to have property "rawHeaders"', () => {
    expect(res).toHaveProperty('rawHeaders');
  });

  test('expect response to have property "socket"', () => {
    expect(res).toHaveProperty('socket');
  });

  test('expect response.body to have property "message"', () => {
    expect(res.body).toHaveProperty('message');
  });

  test('expect response.body.message to have property "name"', () => {
    expect(res.body?.message).toHaveProperty('name');
  });

  test('expect response.body.message to have property "version"', () => {
    expect(res.body?.message).toHaveProperty('version');
  });

  test('expect response.statusCode to be "200"', () => {
    expect(res.statusCode).toBe(200);
  });

  test(`expect response.body.message.name to be "${PKG_NAME}"`, () => {
    expect(res.body?.message.name).toBe(PKG_NAME);
  });

  test(`expect response.body.message.version to be "${PKG_VERSION}"`, () => {
    expect(res.body?.message.version).toBe(PKG_VERSION);
  });
});

describe('Request Module - GET - index -> /path - should follow redirect', () => {
  let res: HttpResponse;

  beforeEach(async () => {
    res = await request('GET', 'http://localhost:8080/path/redirect');
  });

  test('expect response to have property "statusCode"', () => {
    expect(res).toHaveProperty('statusCode');
  });

  test('expect response to have property "body"', () => {
    expect(res).toHaveProperty('body');
  });

  test('expect response to have property "method"', () => {
    expect(res).toHaveProperty('method');
  });

  test('expect response to have property "url"', () => {
    expect(res).toHaveProperty('url');
  });

  test('expect response to have property "statusMessage"', () => {
    expect(res).toHaveProperty('statusMessage');
  });

  test('expect response to have property "headers"', () => {
    expect(res).toHaveProperty('headers');
  });

  test('expect response to have property "rawHeaders"', () => {
    expect(res).toHaveProperty('rawHeaders');
  });

  test('expect response to have property "socket"', () => {
    expect(res).toHaveProperty('socket');
  });

  test('expect response.body to have property "message"', () => {
    expect(res.body).toHaveProperty('message');
  });

  test('expect response.body.message to have property "name"', () => {
    expect(res.body?.message).toHaveProperty('name');
  });

  test('expect response.body.message to have property "version"', () => {
    expect(res.body?.message).toHaveProperty('version');
  });

  test('expect response.statusCode to be "200"', () => {
    expect(res.statusCode).toBe(200);
  });

  test(`expect response.body.message.name to be "${PKG_NAME}"`, () => {
    expect(res.body?.message.name).toBe(PKG_NAME);
  });

  test(`expect response.body.message.version to be "${PKG_VERSION}"`, () => {
    expect(res.body?.message.version).toBe(PKG_VERSION);
  });
});

describe('Request Module - GET - query string param -> /path?id=test', () => {
  let res: HttpResponse;

  beforeEach(async () => {
    res = await request('GET', 'http://localhost:8080/path?id=test');
  });

  test('expect response to have property "statusCode"', () => {
    expect(res).toHaveProperty('statusCode');
  });

  test('expect response to have property "body"', () => {
    expect(res).toHaveProperty('body');
  });

  test('expect response to have property "method"', () => {
    expect(res).toHaveProperty('method');
  });

  test('expect response to have property "url"', () => {
    expect(res).toHaveProperty('url');
  });

  test('expect response to have property "statusMessage"', () => {
    expect(res).toHaveProperty('statusMessage');
  });

  test('expect response to have property "headers"', () => {
    expect(res).toHaveProperty('headers');
  });

  test('expect response to have property "rawHeaders"', () => {
    expect(res).toHaveProperty('rawHeaders');
  });

  test('expect response to have property "socket"', () => {
    expect(res).toHaveProperty('socket');
  });

  test('expect response.body to have property "id"', () => {
    expect(res.body).toHaveProperty('id');
  });

  test('expect response.statusCode to be "200"', () => {
    expect(res.statusCode).toBe(200);
  });

  test(`expect response.body.id to be "test"`, () => {
    expect(res.body?.id).toBe('test');
  });
});

describe('Request Module - POST - JSON payload -> /path', () => {
  let res: HttpResponse;

  const body: Payload = {
    id: 1,
    message: 'POST BODY JSON'
  };

  const headers = {
    'Content-Type': 'application/json'
  };

  beforeEach(async () => {
    res = await request('POST', 'http://localhost:8080/path', {
      body,
      headers
    });
  });

  test('expect response to have property "statusCode"', () => {
    expect(res).toHaveProperty('statusCode');
  });

  test('expect response to have property "body"', () => {
    expect(res).toHaveProperty('body');
  });

  test('expect response to have property "method"', () => {
    expect(res).toHaveProperty('method');
  });

  test('expect response to have property "url"', () => {
    expect(res).toHaveProperty('url');
  });

  test('expect response to have property "statusMessage"', () => {
    expect(res).toHaveProperty('statusMessage');
  });

  test('expect response to have property "headers"', () => {
    expect(res).toHaveProperty('headers');
  });

  test('expect response to have property "rawHeaders"', () => {
    expect(res).toHaveProperty('rawHeaders');
  });

  test('expect response to have property "socket"', () => {
    expect(res).toHaveProperty('socket');
  });

  test('expect response.statusCode to be "200"', () => {
    expect(res.statusCode).toBe(200);
  });

  test('expect response.body to have property "message"', () => {
    expect(res.body).toHaveProperty('message');
  });

  test(`expect response.body.message to be "${body.message}"`, () => {
    expect(res.body?.message).toBe(body.message);
  });
});

describe('Request Module - POST - STRING payload -> /path', () => {
  let res: HttpResponse;

  const body: string = 'POST BODY STRING';

  const headers = {
    'Content-Type': 'text/plain'
  };

  beforeEach(async () => {
    res = await request('POST', 'http://localhost:8080/path', {
      body,
      headers
    });
  });

  test('expect response to have property "statusCode"', () => {
    expect(res).toHaveProperty('statusCode');
  });

  test('expect response to have property "body"', () => {
    expect(res).toHaveProperty('body');
  });

  test('expect response to have property "method"', () => {
    expect(res).toHaveProperty('method');
  });

  test('expect response to have property "url"', () => {
    expect(res).toHaveProperty('url');
  });

  test('expect response to have property "statusMessage"', () => {
    expect(res).toHaveProperty('statusMessage');
  });

  test('expect response to have property "headers"', () => {
    expect(res).toHaveProperty('headers');
  });

  test('expect response to have property "rawHeaders"', () => {
    expect(res).toHaveProperty('rawHeaders');
  });

  test('expect response to have property "socket"', () => {
    expect(res).toHaveProperty('socket');
  });

  test('expect response.statusCode to be "200"', () => {
    expect(res.statusCode).toBe(200);
  });

  test(`expect response.body to be "${body}"`, () => {
    expect(res.body?.message).toBe(body);
  });
});

describe('Request Module - PUT - JSON payload -> /path/:id', () => {
  let res: HttpResponse;

  const body: Payload = {
    id: 1,
    message: 'PUT BODY JSON'
  };

  const headers = {
    'Content-Type': 'application/json'
  };

  beforeEach(async () => {
    res = await request('PUT', `http://localhost:8080/path/${body.id}`, {
      body,
      headers
    });
  });

  test('expect response to have property "statusCode"', () => {
    expect(res).toHaveProperty('statusCode');
  });

  test('expect response to have property "body"', () => {
    expect(res).toHaveProperty('body');
  });

  test('expect response to have property "method"', () => {
    expect(res).toHaveProperty('method');
  });

  test('expect response to have property "url"', () => {
    expect(res).toHaveProperty('url');
  });

  test('expect response to have property "statusMessage"', () => {
    expect(res).toHaveProperty('statusMessage');
  });

  test('expect response to have property "headers"', () => {
    expect(res).toHaveProperty('headers');
  });

  test('expect response to have property "rawHeaders"', () => {
    expect(res).toHaveProperty('rawHeaders');
  });

  test('expect response to have property "socket"', () => {
    expect(res).toHaveProperty('socket');
  });

  test('expect response.statusCode to be "200"', () => {
    expect(res.statusCode).toBe(200);
  });

  test('expect response.body to have property "id"', () => {
    expect(res.body).toHaveProperty('id');
  });

  test('expect response.body to have property "message"', () => {
    expect(res.body).toHaveProperty('message');
  });

  test(`expect response.body.id to be "${body.id}"`, () => {
    expect(res.body?.id).toBe(body.id);
  });

  test(`expect response.body.message to be "${body.message}"`, () => {
    expect(res.body?.message).toBe(body.message);
  });
});

describe('Request Module - DELETE - delete one at -> /path/:id', () => {
  let res: HttpResponse;

  const body: Payload = {
    id: 1
  };

  beforeEach(async () => {
    res = await request('DELETE', 'http://localhost:8080/path/1', {
      body
    });
  });

  test('expect response to have property "statusCode"', () => {
    expect(res).toHaveProperty('statusCode');
  });

  test('expect response to have property "body"', () => {
    expect(res).toHaveProperty('body');
  });

  test('expect response to have property "method"', () => {
    expect(res).toHaveProperty('method');
  });

  test('expect response to have property "url"', () => {
    expect(res).toHaveProperty('url');
  });

  test('expect response to have property "statusMessage"', () => {
    expect(res).toHaveProperty('statusMessage');
  });

  test('expect response to have property "headers"', () => {
    expect(res).toHaveProperty('headers');
  });

  test('expect response to have property "rawHeaders"', () => {
    expect(res).toHaveProperty('rawHeaders');
  });

  test('expect response to have property "socket"', () => {
    expect(res).toHaveProperty('socket');
  });

  test('expect response.body to have property "id"', () => {
    expect(res.body).toHaveProperty('id');
  });

  test('expect response.statusCode to be "200"', () => {
    expect(res.statusCode).toBe(200);
  });

  test(`expect response.body.id to be "${body.id}"`, () => {
    expect(res.body?.id).toBe(body.id);
  });
});
